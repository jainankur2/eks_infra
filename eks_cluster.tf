module "eks_cluster" {
  source                      = "./modules/eks_cluster"
  region                      = var.region
  dns_name                    = var.dns_name
  account_environment         = var.account_environment
  availability_zones          = var.availability_zones
  vpc_id                      = module.backend_infra.vpc_id
  private_subnet_ids          = module.backend_infra.private_subnet_ids
  db_subnet_ids               = module.backend_infra.db_subnet_ids
  ami_type                    = var.ami_type
  disk_size                   = var.disk_size
  instance_types              = var.instance_types
  desired_size                = var.desired_size
  max_size                    = var.max_size
  min_size                    = var.min_size
  rds_cluster_instance_class  = var.rds_cluster_instance_class
  rds_cluster_instance_number = var.rds_cluster_instance_number
}
