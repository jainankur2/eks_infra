pipeline {
    agent any

    parameters {
        string(name: 'environment', defaultValue: 'default', description: 'Workspace/environment file to use for deployment')
        booleanParam(name: 'autoApprove', defaultValue: false, description: 'Automatically run apply after generating plan?')
    }
    environment{
        TERRAFORM_VERSION="0.14.1"
    }
    stages {
        stage('Plan') {
            agent {
                docker {
                    image "hashicorp/terraform:${TERRAFORM_VERSION}"
                    args '--entrypoint=""'
                }
            }
            steps {
                sh "terraform init -backend-config=\"environment/${params.environment}.backend.tfvars\" --reconfigure --upgrade"
                sh "terraform plan -var-file=\"environment/${params.environment}.tfvars\" -no-color -out tfplan | tee tfplan_state"
                sh 'terraform show -no-color tfplan > tfplan.txt'
                
            }
        }

        stage('Approval') {
            when {
                not {
                    equals expected: true, actual: params.autoApprove
                }
            }

            steps {
                script {
                    def plan = readFile 'tfplan.txt'
                    input message: "Do you want to apply the plan?",
                        parameters: [text(name: 'Plan', description: 'Please review the plan', defaultValue: plan)]
                }
            }
        }

        stage('Apply') {
            agent {
                docker {
                    image "hashicorp/terraform:${TERRAFORM_VERSION}"
                    args '--entrypoint=""'
                }
            }
            steps {
                sh "terraform apply -input=false tfplan"
            }
        }
    }

    post {
        always {
            archiveArtifacts artifacts: 'tfplan.txt'
        }
    }
}
