region = "us-east-2"

#Environment information
dns_name = "accolite"
vpc_cidr = "198.10.0.0/16"
pub1_cidr = "198.10.0.0/24"
pub2_cidr = "198.10.1.0/24"
priv1_cidr = "198.10.2.0/24"
priv2_cidr = "198.10.3.0/24"
db1_cidr = "198.10.4.0/24"
db2_cidr = "198.10.5.0/24"
account_environment = "dev"
availability_zones = [
  "us-east-2a",
  "us-east-2b",
  "us-east-2c"
]
rds_cluster_instance_class = "db.t3.medium"
rds_cluster_instance_number = "2"
ami_type = "AL2_x86_64"
disk_size = "20"
instance_types = ["t3.medium"]
desired_size = "1"
max_size = "1"
min_size = "0"
