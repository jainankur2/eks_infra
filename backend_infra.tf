module "backend_infra" {
  source                      = "./modules/backend_infra"
  region                      = var.region
  dns_name                    = var.dns_name
  vpc_cidr                    = var.vpc_cidr
  pub1_cidr                   = var.pub1_cidr
  pub2_cidr                   = var.pub2_cidr
  priv1_cidr                  = var.priv1_cidr
  priv2_cidr                  = var.priv2_cidr
  db1_cidr                    = var.db1_cidr
  db2_cidr                    = var.db2_cidr
  account_environment         = var.account_environment
  availability_zones          = var.availability_zones
  rds_cluster_instance_class  = var.rds_cluster_instance_class
  rds_cluster_instance_number = var.rds_cluster_instance_number
}
