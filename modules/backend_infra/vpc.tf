resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr
  tags       = merge(local.common_tags, { "Name" = "${local.name_suffix}-vpc" })
}

resource "aws_subnet" "public-subnet-1" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.pub1_cidr
  map_public_ip_on_launch = "true"
  availability_zone       = var.availability_zones[0]
  tags                    = merge(local.common_tags, { "Name" = "${local.name_suffix}-public-subnet-1" })
}

resource "aws_subnet" "public-subnet-2" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.pub2_cidr
  map_public_ip_on_launch = "true"
  availability_zone       = var.availability_zones[1]
  tags                    = merge(local.common_tags, { "Name" = "${local.name_suffix}-public-subnet-2" })
}

resource "aws_subnet" "private-subnet-1" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.priv1_cidr
  map_public_ip_on_launch = "false"
  availability_zone       = var.availability_zones[0]
  tags                    = merge(local.common_tags, { "Name" = "${local.name_suffix}-private-subnet-1" })
}

resource "aws_subnet" "private-subnet-2" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.priv2_cidr
  map_public_ip_on_launch = "false"
  availability_zone       = var.availability_zones[1]
  tags                    = merge(local.common_tags, { "Name" = "${local.name_suffix}-private-subnet-2" })
}

resource "aws_subnet" "db-subnet-1" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.db1_cidr
  map_public_ip_on_launch = "false"
  availability_zone       = var.availability_zones[0]
  tags                    = merge(local.common_tags, { "Name" = "${local.name_suffix}-db-subnet-1" })
}

resource "aws_subnet" "db-subnet-2" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.db2_cidr
  map_public_ip_on_launch = "false"
  availability_zone       = var.availability_zones[1]
  tags                    = merge(local.common_tags, { "Name" = "${local.name_suffix}-db-subnet-2" })
}

