output "rds-endpoint" {
  value = aws_rds_cluster.rds-cluster.endpoint
}

output "vpc_id" {
  value = aws_vpc.main.id
}

output "private_subnet_ids" {
  value = [aws_subnet.private-subnet-1.id, aws_subnet.private-subnet-2.id]
}

output "db_subnet_ids" {
  value = [aws_subnet.db-subnet-1.id, aws_subnet.db-subnet-2.id]
}

