resource "aws_db_subnet_group" "rds-subnet-group" {
  name        = format("%s-%s-doms", var.dns_name, var.account_environment)
  subnet_ids  = [aws_subnet.db-subnet-1.id, aws_subnet.db-subnet-2.id]
  description = "Mysql Subnet group"
  tags        = merge(local.common_tags, { "Name" = "${local.name_suffix}-db-subnet-group" })
}

resource "random_password" "rds_password" {
  length  = 12
  special = false
  #  override_special = "!#()-[]<>\""
}

resource "aws_rds_cluster" "rds-cluster" {
  cluster_identifier           = format("%s-%s-rds-cluster", var.dns_name, var.account_environment)
  master_username              = "admin"
  master_password              = random_password.rds_password.result
  engine                       = "aurora-mysql"
  engine_version               = "5.7.mysql_aurora.2.07.2"
  preferred_maintenance_window = "Sun:04:00-Sun:06:00"
  preferred_backup_window      = "02:00-04:00"
  backup_retention_period      = 14
  storage_encrypted            = false
  port                         = 3306
  db_subnet_group_name         = aws_db_subnet_group.rds-subnet-group.name
  deletion_protection          = false
  skip_final_snapshot          = true
  #  vpc_security_group_ids = [
  #  aws_security_group.rds-server-sg.id
  #  ]
  tags = merge(local.common_tags, { "Name" = "${local.name_suffix}-rds-cluster" })
}

resource "aws_rds_cluster_instance" "rds-cluster-instance" {
  count                      = var.rds_cluster_instance_number
  identifier                 = format("%s-%s-cluster-db-%s", var.dns_name, var.account_environment, count.index + 1)
  cluster_identifier         = aws_rds_cluster.rds-cluster.id
  instance_class             = var.rds_cluster_instance_class
  auto_minor_version_upgrade = true
  publicly_accessible        = false
  engine                     = "aurora-mysql"
  engine_version             = "5.7.mysql_aurora.2.07.2"
  tags                       = merge(local.common_tags, { "Name" = "${local.name_suffix}-rds-cluster-instance" })
}
