locals {
  name_suffix = "${var.dns_name}-${var.account_environment}"
  common_tags = {
    environment = var.account_environment,
    account     = var.dns_name
  }
}
